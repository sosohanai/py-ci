import openai
import pandas as pd
import requests
import feedparser
from deep_translator import GoogleTranslator
from redminelib import Redmine
from datetime import datetime
# from langchain.prompts import PromptTemplate
# from langchain.chat_models import ChatOpenAI
# from langchain.chains import LLMChain
from langchain_core.runnables import RunnableSequence
# from langchain_community.llms import OpenAI
from langchain_openai import OpenAI
from langchain_core.prompts import prompt
from langchain_core.runnables import RunnableLambda
from langchain_core.output_parsers import StrOutputParser
from langchain_core.prompts import ChatPromptTemplate
# pip install openai pandas requests feedparser deep-translator python-redmine langchain langchain-openai

# OpenAI API 키 설정
OPENAI_API_KEY = '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'
openai.api_key = OPENAI_API_KEY

# Redmine API 설정
redmine_url = 'https://red.sosoa.duckdns.org'
api_key = 'b6c56a70e468abacaf72252a744c58d47611be38'
project_id = 'py-ci'
redmine = Redmine(redmine_url, key=api_key)

# Google 번역기 설정
translator = GoogleTranslator(source='en', target='ko')

def fetch_latest_arxiv_papers(category="cs.AI", max_results=3):
    base_url = f"https://export.arxiv.org/api/query?search_query=cat:{category}&sortBy=submittedDate&sortOrder=descending&max_results={max_results}"
    feed = feedparser.parse(base_url)

    papers = []
    for entry in feed.entries:
        title = entry.title
        abstract = entry.summary
        url = entry.link
        published = entry.published
        papers.append({
            "title": title,
            "abstract": abstract,
            "url": url,
            "published": published
        })

    return papers

def save_papers_to_csv(papers, file_path):
    df = pd.DataFrame(papers)
    df.to_csv(file_path, index=False, encoding='utf-8')

def summarize_paper_list(papers):
    summaries = []
    for paper in papers:
        title = paper['title']
        abstract = paper['abstract']
        summaries.append(f"제목: {title}\n초록: {abstract}\n")
    combined_text = "\n".join(summaries)

    # LangChain 사용하여 요약
    # prompt_template = PromptTemplate(
    #     input_variables=["text"],
    #     template="Please summarize the following research papers:\n\n{text}"
    # )
    # chain = LLMChain(prompt=prompt_template, llm=llm)

    prompt = ChatPromptTemplate.from_messages([
        ("system", "You are a world class technical documentation writer."),
        ("user", f"Please summarize the following research papers:\n\n{combined_text}")
    ])
    # prompt_text = "Please summarize the following research papers:" + combined_text
    llm = OpenAI(api_key=OPENAI_API_KEY)
    # # Create a RunnableSequence
    output_parser = StrOutputParser()
    chain = prompt | llm | output_parser
    # chain.invoke(prompt)
    # prompt_runnable = RunnableLambda(lambda: prompt_text)
    response = chain.invoke({"input": prompt})
    english_summary = response
    # korean_summary = translator.translate(english_summary)

    # combined_summary = f"{english_summary}\n\n한국어 요약:\n{korean_summary}"

    return english_summary

def post_to_redmine_news(redmine, project_id, news_title, news_description):
    news = redmine.news.create(
        project_id=project_id,
        title=news_title,
        description=news_description
    )
    if news:
        print("뉴스가 성공적으로 게시되었습니다.")
        print(news)
    else:
        print("뉴스 게시 실패")

# 최신 3개 논문 스크랩
latest_papers = fetch_latest_arxiv_papers(max_results=3)

# CSV 파일로 저장
save_papers_to_csv(latest_papers, 'latest_arxiv_papers.csv')

# 논문 목록 요약
combined_summary = summarize_paper_list(latest_papers)

# Redmine 게시
current_date = datetime.now().strftime("%Y-%m-%d")
news_title = f'최근 아카이브 논문 요약 - {current_date}'
news_description = combined_summary

post_to_redmine_news(redmine, project_id, news_title, news_description)
