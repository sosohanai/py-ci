# 레드마인 프로젝트 뉴스에 최신 논문 요약 게시
# 최신 논문을 아카이브에서 스크랩하고, GPT-3를 사용하여 요약한 후 레드마인 프로젝트 뉴스에 게시
# pip install openai pandas requests feedparser deep-translator python-redmine
from openai import OpenAI
import pandas as pd
import requests
import feedparser
from deep_translator import GoogleTranslator
from redminelib import Redmine

# OpenAI API 키 설정
client = OpenAI(api_key='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')

# Redmine API 설정
redmine_url = 'https://red.sosoa.duckdns.org'
api_key = 'b6c56a70e468abacaf72252a744c58d47611be38'
redmine = Redmine(redmine_url, key=api_key)
project_id = 'py-ci'
# Google 번역기 설정
translator = GoogleTranslator(source='en', target='ko')


def fetch_latest_arxiv_papers(category="cs.AI", max_results=3):
    base_url = f"https://export.arxiv.org/api/query?search_query=cat:{category}&sortBy=submittedDate&sortOrder=descending&max_results={max_results}"
    feed = feedparser.parse(base_url)

    papers = []
    for entry in feed.entries:
        title = entry.title
        abstract = entry.summary
        url = entry.link
        published = entry.published
        papers.append({
            "title": title,
            "abstract": abstract,
            "url": url,
            "published": published
        })

    return papers


def save_papers_to_csv(papers, file_path):
    df = pd.DataFrame(papers)
    df.to_csv(file_path, index=False, encoding='utf-8')


def summarize_paper_list(papers):
    summaries = []
    for paper in papers:
        title = paper['title']
        abstract = paper['abstract']
        summaries.append(f"제목: {title}\n초록: {abstract}\n")
    combined_text = "\n".join(summaries)

    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant that summarizes research papers."},
            {"role": "user", "content": f"다음 논문 목록을 요약해 주세요:\n\n{combined_text}"}
        ],
        # max_tokens=16385,
        # temperature=0.5,
    )

    # english_summary = response.choices[0].message['content'].strip()
    english_summary = response.choices[0].message.content.strip()
    korean_summary = translator.translate(english_summary, src='en', dest='ko').strip()
    korean_full = translator.translate(combined_text, src='en', dest='ko').strip()

    # combined_summary = f"{english_summary}\n\n한국어 요약:\n{korean_summary}"
    combined_summary = f"한국어 요약:\n{korean_summary}\n\n[초록]\n\n {korean_full}\n\n{english_summary}\n\n[초록]\n\n {combined_text}\n"
    # combined_summary = korean_summary
    # return response.choices[0].message['content'].strip()
    return combined_summary


def post_to_redmine_news(redmine, project_id, news_title, news_description):
    news = redmine.news.create(
        project_id=project_id,
        title=news_title,
        description=news_description
    )

    if news:
        print("뉴스가 성공적으로 게시되었습니다.")
        print(news)
    else:
        print("뉴스 게시 실패")



# 최신 3개 논문 스크랩
latest_papers = fetch_latest_arxiv_papers(max_results=3)

# CSV 파일로 저장
save_papers_to_csv(latest_papers, 'latest_arxiv_papers.csv')

# 논문 목록 요약
combined_summary = summarize_paper_list(latest_papers)

# Redmine 게시
news_title = '최근 아카이브 논문 요약'
news_description = combined_summary

post_to_redmine_news(redmine, project_id, news_title, news_description)
