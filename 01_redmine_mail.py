# pip install pillow selenium webdriver-manager
import time
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

# 레드마인 설정
REDMINE_URL = 'https://red.sosoa.duckdns.org'  # 레드마인 URL
API_KEY = 'b6c56a70e468abacaf72252a744c58d47611be38'  # 레드마인 API 키
PROJECT_ID = 'py-ci'  # 레드마인 프로젝트 ID
GANTT_URL = f'{REDMINE_URL}/issues/gantt'
# https://red.sosoa.duckdns.org/issues/gantt
# 이메일 설정
SMTP_SERVER = 'smtp.gmail.com'  # SMTP 서버 주소
SMTP_PORT = 587  # SMTP 포트
EMAIL_ADDRESS = 'sosohanekl@gmail.com'  # 송신자 이메일 주소
EMAIL_PASSWORD = 'wmhq oktr zpch nknz'  # 송신자 이메일 비밀번호
TO_ADDRESS = 'sosohanekl@gmail.com'  # 수신자 이메일 주소

# 간트차트 PNG 파일 경로
PNG_PATH = 'gantt_chart.png'


def fetch_gantt_chart():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--window-size=1920x1080")

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
    driver.get(GANTT_URL)

    # 로그인 등이 필요한 경우, 해당 절차를 추가
    # driver.find_element(By.ID, "username").send_keys("your_username")
    # driver.find_element(By.ID, "password").send_keys("your_password")
    # driver.find_element(By.NAME, "login").click()

    time.sleep(5)  # 페이지 로딩 대기
    driver.save_screenshot(PNG_PATH)
    driver.quit()


def send_email():
    msg = MIMEMultipart()
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = TO_ADDRESS
    msg['Subject'] = f"Redmine Gantt Chart - {datetime.now().strftime('%Y-%m-%d')}"

    body = "Please find the attached Gantt chart for the project."
    msg.attach(MIMEText(body, 'plain'))

    with open(PNG_PATH, 'rb') as file:
        attach = MIMEApplication(file.read(), _subtype='png')
        attach.add_header('Content-Disposition', 'attachment', filename='gantt_chart.png')
        msg.attach(attach)

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
    text = msg.as_string()
    server.sendmail(EMAIL_ADDRESS, TO_ADDRESS, text)
    server.quit()


def main():
    fetch_gantt_chart()
    send_email()


if __name__ == "__main__":
    main()
