#!/bin/bash

# URL of the health endpoint
HEALTH_ENDPOINT_URL="http://localhost:8080/health"

# Make a GET request to the health endpoint and store the HTTP status code in a variable
http_status=$(curl -s -o /dev/null -w "%{http_code}" "$HEALTH_ENDPOINT_URL")

# Check if the HTTP status code is 200 (OK)
if [ "$http_status" -eq 200 ]; then
    echo "Health check passed: Application is healthy."
else
    echo "Health check failed: Application is not healthy (HTTP status code: $http_status)."
    exit 1  # Exit with a non-zero status code to indicate failure
fi
