import re
import nltk
from nltk.corpus import stopwords
#pip install nltk
nltk.download('stopwords')
stop_words = set(stopwords.words('english'))

def preprocess_text(text):
    # 소문자 변환
    text = text.lower()
    # 특수 문자 제거
    text = re.sub(r'\W', ' ', text)
    # 단어 단위로 분할
    words = text.split()
    # 불용어 제거
    words = [word for word in words if word not in stop_words]
    return ' '.join(words)

df['cleaned_review'] = df['review'].apply(preprocess_text)
df.head()