import requests
from openai import OpenAI
import pandas as pd

# 설정
ARXIV_API_URL = 'http://export.arxiv.org/api/query'
API_KEY = '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'

client = OpenAI(api_key=API_KEY)

# arXiv에서 논문 목록 가져오기
def fetch_arxiv_papers(query, max_results=5):
    params = {
        'search_query': query,
        'start': 0,
        'max_results': max_results
    }
    response = requests.get(ARXIV_API_URL, params=params)
    response.raise_for_status()
    return response.text

# 논문 요약 및 성능 수치 수집하기
def summarize_paper(title, abstract):
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[
            {"role": "system", "content": "You are a helpful assistant."},
            {"role": "user", "content": f"Summarize this paper and provide benchmark scores and applicable examples:\n\nTitle: {title}\n\nAbstract: {abstract}"}
        ]
    )
    return response.choices[0].message.content

# arXiv에서 논문 목록 가져오기
query = 'cat:cs.AI'  # 예제: 인공지능 카테고리의 논문
arxiv_response = fetch_arxiv_papers(query)
papers = arxiv_response.split('<entry>')[1:]  # 각 논문은 <entry> 태그로 시작

# 논문 요약 및 성능 수치 수집
summaries = []
for paper in papers:
    title_start = paper.find('<title>') + len('<title>')
    title_end = paper.find('</title>')
    title = paper[title_start:title_end].strip()

    abstract_start = paper.find('<summary>') + len('<summary>')
    abstract_end = paper.find('</summary>')
    abstract = paper[abstract_start:abstract_end].strip()

    summary = summarize_paper(title, abstract)
    summaries.append((title, summary))

# 데이터프레임으로 변환
df = pd.DataFrame(summaries, columns=['Title', 'Summary'])
print(df)