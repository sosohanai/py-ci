import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from datetime import datetime

# 이메일 설정
SMTP_SERVER = 'smtp.gmail.com'
SMTP_PORT = 587
EMAIL_ADDRESS = 'your_email@gmail.com'
EMAIL_PASSWORD = 'your_email_password'
TO_ADDRESS = 'recipient_email@gmail.com'

def create_html_table(performance_df):
    html = '<table border="1">'
    html += '<tr><th>Title</th><th>Metrics</th><th>Applicable Examples</th></tr>'
    for _, row in performance_df.iterrows():
        metrics_str = "<br>".join([f"{k}: {v}" for k, v in row['Metrics'].items()])
        examples_str = "<br>".join(row['Applicable Examples'])
        html += f'<tr><td>{row["Title"]}</td><td>{metrics_str}</td><td>{examples_str}</td></tr>'
    html += '</table>'
    return html

def send_email(subject, body):
    msg = MIMEMultipart()
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = TO_ADDRESS
    msg['Subject'] = subject

    msg.attach(MIMEText(body, 'html'))

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
    text = msg.as_string()
    server.sendmail(EMAIL_ADDRESS, TO_ADDRESS, text)
    server.quit()

html_table = create_html_table(performance_df)
email_subject = f"Research Paper Performance Report - {datetime.now().strftime('%Y-%m-%d')}"
email_body = f"<h2>Research Paper Performance Report</h2>{html_table}"

send_email(email_subject, email_body)
print("Email sent successfully.")