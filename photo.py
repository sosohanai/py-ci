from flask import Blueprint

upload_bp = Blueprint('photo', __name__)


@upload_bp.route('/photo')
def index():
    # 파일 업로드 기능 구현
    return 'photo page'
