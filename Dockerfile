# Use an official Python runtime as a parent image
FROM python:3.8-slim-buster

# Install sshpass and other dependencies
RUN apt-get update && apt-get install -y sshpass openssh-client && rm -rf /var/lib/apt/lists/*


# Set the working directory in the container to /app
WORKDIR /app

# Add the current directory contents into the container at /app
ADD . /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Make port 8091 available to the world outside this container
EXPOSE 8091

# Run app.py when the container launches
CMD ["python", "app.py"]