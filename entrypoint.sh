#!/bin/sh

# SSH_PRIVATE_KEY 환경 변수를 사용하여 SSH 키 설정
echo "$SSH_PRIVATE_KEY" | tr -d '\r' > /root/.ssh/id_rsa
chmod 600 /root/.ssh/id_rsa
pwd
ls -al
# SSH 공개 키 설정
echo "$SSH_PUBLIC_KEY" > /root/.ssh/id_rsa.pub
chmod 644 /root/.ssh/id_rsa.pub
pwd
ls -al
# SSH 호스트 키를 known_hosts 파일에 추가
ssh-keyscan -p $SSH_PORT $SSH_HOST >> /root/.ssh/known_hosts
pwd
ls -al
echo $SSH_PRIVATE_KEY
echo $SSH_PUBLIC_KEY
# 접근 가능한 경로로 파일 복사 (예: /tmp)
#ssh -i /root/.ssh/id_rsa -p $SSH_PORT $SSH_USER@$SSH_HOST "cp /path/to/file /tmp && sudo chmod 777 /tmp/file"

# 원격 서버에 SSH 키 복사
#sshpass -p "$SSH_PASS" ssh-copy-id -i /root/.ssh/id_rsa.pub $SSH_USER@$SSH_HOST

# 시작 스크립트 실행
exec "$@"