from redminelib import Redmine

# Redmine 서버 URL 및 API 키 설정
redmine = Redmine('https://red.sosoa.duckdns.org', key='b6c56a70e468abacaf72252a744c58d47611be38')

# 프로젝트 ID 설정
# project_id = 'py-ci'
project_id = 'infra'

# 프로젝트 작업 가져오기
issues = redmine.issue.filter(project_id=project_id)
# # 이슈 작업 가져오기
# issues = redmine.issue.filter(tracker_id=3)
# 변경할 작업 유형 ID 설정
# new_tracker_id = 4 # 환경
# new_tracker_id = 2 # 새기능
# new_tracker_id = 3 # 지원
new_issue_status_id = 2 # 진행중

# 모든 작업의 유형 변경
for issue in issues:
    # 유형 변경
    # issue.tracker_id = new_tracker_id
    # issue.save()
    # 상태 변경
    issue.status_id = new_issue_status_id
    issue.save()

    # 유형 삭제
    # issue.delete()
    # print(f"Issue {issue.id}의 유형이 변경되었습니다.")
    print(f"Issue {issue.id}의 상태가 변경되었습니다.")
