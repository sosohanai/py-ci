from flask import Flask, url_for
from photo import upload_bp

app = Flask(__name__)
app.register_blueprint(upload_bp)


@app.route('/')
def hello_world():
    return 'Hello, modified Python this is 설치형 gitlab ce! <a href="' + url_for('photo.index') + '">Go to Photo</a>'


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8091)
