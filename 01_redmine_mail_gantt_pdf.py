import time
import base64
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from PyPDF2 import PdfReader, PdfWriter
from selenium.webdriver.common.by import By
from PIL import Image
from pdf2image import convert_from_path
import fitz  # PyMuPDF
from email.mime.application import MIMEApplication
# 패키지 설치
# pip install PyPDF2 selenium webdriver_manager Pillow pdf2image PyMuPDF
# 맥의 경우 brew install poppler
# 확인 which pdftoppm

# 레드마인 설정
REDMINE_URL = 'https://red.sosoa.duckdns.org'  # 레드마인 URL
API_KEY = 'b6c56a70e468abacaf72252a744c58d47611be38'  # 레드마인 API 키
PROJECT_ID = 'py-ci'  # 레드마인 프로젝트 ID
GANTT_URL = f'{REDMINE_URL}/issues/gantt?query_id=12' # 진척도검색포함
# https://red.sosoa.duckdns.org/issues/gantt
# 이메일 설정
SMTP_SERVER = 'smtp.gmail.com'  # SMTP 서버 주소
SMTP_PORT = 587  # SMTP 포트
EMAIL_ADDRESS = 'sosohanekl@gmail.com'  # 송신자 이메일 주소
EMAIL_PASSWORD = 'wmhq oktr zpch nknz'  # 송신자 이메일 비밀번호
TO_ADDRESS = 'sosohanekl@gmail.com'  # 수신자 이메일 주소

# 간트차트 PDF 파일 경로
PDF_PATH = 'gantt_chart.pdf'
CROPPED_PDF_PATH = 'cropped_gantt_chart.pdf'



def fetch_gantt_chart():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--window-size=1920x1080")

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
    driver.get(GANTT_URL)

    time.sleep(5)  # 페이지 로딩 대기

    # 페이지를 PDF로 인쇄
    print_options = {
        'printBackground': True,
        'landscape': False,
        'displayHeaderFooter': False,
        'printToFile': True,
        'filePath': PDF_PATH
    }
    # Execute the command to print the page to a PDF
    result = driver.execute_cdp_cmd('Page.printToPDF', print_options)

    # The result is a dictionary with a single key 'data', which contains the base64-encoded PDF data
    pdf_data_base64 = result['data']

    # Decode the base64 data to get the binary PDF data
    pdf_data_binary = base64.b64decode(pdf_data_base64)

    # Write the binary data to a file
    with open(PDF_PATH, 'wb') as file:
        file.write(pdf_data_binary)

    driver.quit()

    # PDF 자르기
    with open(PDF_PATH, 'rb') as file:
        reader = PdfReader(file)
        page = reader.pages[0]

        # 간트 차트가 있는 영역을 자릅니다.
        # 여기서는 예시로 전체 페이지를 자르지 않고 그대로 사용합니다.
        # 실제로는 간트 차트가 있는 영역의 좌표를 지정해야 합니다.
        page.cropbox.lower_left = (0, 0)
        page.cropbox.upper_right = (page.mediabox.upper_right[0], page.mediabox.upper_right[1])


        writer = PdfWriter()
        writer.add_page(page)

        with open(CROPPED_PDF_PATH, 'wb') as output_file:
            writer.write(output_file)


def send_email():
    msg = MIMEMultipart()
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = TO_ADDRESS
    msg['Subject'] = f"Redmine Gantt Chart - {datetime.now().strftime('%Y-%m-%d')}"

    # Convert the cropped PDF to a list of image objects
    images = convert_from_path(CROPPED_PDF_PATH)

    # Save the first image to a PNG file
    png_path = 'cropped_gantt_chart.png'
    images[0].save(png_path, 'PNG')

    with open(png_path, 'rb') as file:
        img = MIMEImage(file.read())
        img.add_header('Content-ID', '<{}>'.format(png_path))  # Add Content-ID header
        msg.attach(img)

    # body = "Please find the attached Gantt chart for the project."
    # msg.attach(MIMEText(body, 'plain'))
    # Include the image in the email body
    body = f"""
       <p>Please find the Gantt chart for the project below:</p>
       <img src="cid:{png_path}" alt="Gantt chart">
       """
    msg.attach(MIMEText(body, 'html'))

    # Convert the cropped PDF to a list of image objects
    images = convert_from_path(CROPPED_PDF_PATH)

    # Save the first image to a PNG file
    # If the PDF has multiple pages, this will only save the first page
    # If you want to save all pages, you can loop over the images and save each one
    png_path = 'cropped_gantt_chart.png'
    images[0].save(png_path, 'PNG')

    with open(CROPPED_PDF_PATH, 'rb') as file:
        pdf_data_binary = file.read()
        pdf_document = fitz.open(stream=pdf_data_binary)
        for page in pdf_document:
            # linux
            # fontfile = '/src/cron/D2Coding/D2Coding-Ver1.3.2-20180524.ttf'
            # mac
            fontfile = '/Users/Shared/fonts/D2Coding/D2Coding-Ver1.3.2-20180524.ttf'
            # Set optional parameters to default values
            fontbuffer = None #None  # 이 값은 폰트 파일의 내용을 담는 데 사용됩니다. 여기서는 None을 사용하여 파일에서 직접 읽어오도록 합니다.
            set_simple = 0 #0  # 이 값은 간단한 폰트 설정을 사용할지 여부를 결정합니다. 0은 '아니오'를 의미합니다.
            idx = 0 #0  # 이 값은 폰트 파일 내의 폰트 인덱스를 지정합니다. 대부분의 폰트 파일에는 하나의 폰트만 있으므로 0을 사용합니다.
            wmode = 0 #0  # 이 값은 폰트의 쓰기 모드를 설정합니다. 0은 가로 쓰기를 의미합니다.
            serif = 0 #0  # 이 값은 세리프 폰트를 사용할지 여부를 결정합니다. 0은 '아니오'를 의미합니다.
            encoding = 0 #'utf-8'  # 이 값은 폰트의 인코딩을 설정합니다. 빈 문자열은 기본 인코딩을 사용하겠다는 의미입니다.
            ordering = 0 #''  # 이 값은 폰트의 문자 정렬 방식을 설정합니다. 빈 문자열은 기본 정렬 방식을 사용하겠다는 의미입니다.
            force = False #1  # 이 값은 폰트 삽입을 강제할지 여부를 결정합니다. 1은 '예'를 의미합니다.

            # Insert font
            page._insertFont('D2Coding', fontfile, fontbuffer, set_simple, idx, wmode, serif, encoding, ordering, force)


        modified_pdf_buffer = pdf_document.write()


        attach = MIMEApplication(modified_pdf_buffer, _subtype='pdf')
        attach.add_header('Content-Disposition', 'attachment', filename='cropped_gantt_chart.pdf')
        msg.attach(attach)

    # with open(png_path, 'rb') as file:
    #     img = MIMEImage(file.read())
    #     img.add_header('Content-Disposition', 'attachment', filename='cropped_gantt_chart.png')
    #     msg.attach(img)

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
    text = msg.as_string()
    server.sendmail(EMAIL_ADDRESS, TO_ADDRESS, text)
    server.quit()


def main():
    fetch_gantt_chart()
    send_email()


if __name__ == "__main__":
    main()