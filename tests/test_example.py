
# test_example.py

import unittest
from example import add_numbers

class TestExample(unittest.TestCase):
    def test_add_numbers(self):
        # Test case 1: Test addition of positive numbers
        self.assertEqual(add_numbers(2, 3), 5)

        # Test case 2: Test addition of negative numbers
        self.assertEqual(add_numbers(-2, -3), -5)

        # Test case 3: Test addition of a positive and a negative number
        self.assertEqual(add_numbers(5, -3), 2)

if __name__ == '__main__':
    unittest.main()
