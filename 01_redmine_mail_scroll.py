import time
import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from email.mime.application import MIMEApplication
from email.mime.image import MIMEImage
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options
from PIL import Image

# 레드마인 설정
REDMINE_URL = 'https://red.sosoa.duckdns.org'  # 레드마인 URL
API_KEY = 'b6c56a70e468abacaf72252a744c58d47611be38'  # 레드마인 API 키
PROJECT_ID = 'py-ci'  # 레드마인 프로젝트 ID
GANTT_URL = f'{REDMINE_URL}/issues/gantt'

# 이메일 설정
SMTP_SERVER = 'smtp.gmail.com'  # SMTP 서버 주소
SMTP_PORT = 587  # SMTP 포트
EMAIL_ADDRESS = 'sosohanekl@gmail.com'  # 송신자 이메일 주소
EMAIL_PASSWORD = 'wmhq oktr zpch nknz'  # 송신자 이메일 비밀번호
TO_ADDRESS = 'sosohanekl@gmail.com'  # 수신자 이메일 주소

# 추가할 CSS 파일 URL 리스트
CSS_URLS = [
    'https://red.sosoa.duckdns.org/stylesheets/jquery/jquery-ui-1.13.2.css',
    'https://red.sosoa.duckdns.org/stylesheets/application.css',
    'https://red.sosoa.duckdns.org/stylesheets/responsive.css',
    'https://red.sosoa.duckdns.org/stylesheets/redmine_agile.css',
    'https://red.sosoa.duckdns.org/stylesheets/redmine_category_tree.css',
    'https://red.sosoa.duckdns.org/stylesheets/content.css'
]

# 추가할 JavaScript 파일 URL 리스트
JS_URLS = [
    'https://red.sosoa.duckdns.org/javascripts/jquery-3.6.1-ui-1.13.2-ujs-6.1.7.6.js',
    'https://red.sosoa.duckdns.org/javascripts/tablesort.min.js',
    'https://red.sosoa.duckdns.org/javascripts/tablesort-5.2.1.number.min.js',
    'https://red.sosoa.duckdns.org/javascripts/application.js',
    'https://red.sosoa.duckdns.org/javascripts/responsive.js',
    'https://red.sosoa.duckdns.org/javascripts/theme.js'
]

def fetch_gantt_chart_html():
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument("--disable-gpu")
    chrome_options.add_argument("--no-sandbox")
    chrome_options.add_argument("--window-size=1920x1080")

    driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)
    driver.get(GANTT_URL)

    time.sleep(5)  # 페이지 로딩 대기

    # 불필요한 요소 제거
    driver.execute_script("""
        document.getElementById('sidebar').remove();
        document.getElementById('query_form').remove();
        document.getElementById('top-menu').remove();
        document.getElementById('header').remove();
    """)

    # 필요한 요소의 HTML 추출
    gantt_subjects_element = driver.find_element(By.CLASS_NAME, 'gantt_subjects')
    gantt_html = gantt_subjects_element.get_attribute('outerHTML')

    # CSS 링크 태그 생성
    style_html = ""
    for css_url in CSS_URLS:
        style_html += f'<link rel="stylesheet" type="text/css" href="{css_url}">\n'

    # JavaScript 태그 생성
    script_html = ""
    for js_url in JS_URLS:
        script_html += f'<script src="{js_url}"></script>\n'

    return driver, style_html, script_html, gantt_html

def capture_screenshots(driver):
    screenshots = []
    total_height = driver.execute_script("return document.body.scrollHeight")
    viewport_height = driver.execute_script("return window.innerHeight")

    num_screenshots = total_height // viewport_height + 1

    for i in range(num_screenshots):
        driver.execute_script(f"window.scrollTo(0, {i * viewport_height});")
        time.sleep(2)  # 스크롤 후 대기 시간
        screenshot = driver.get_screenshot_as_png()
        screenshots.append(screenshot)

    driver.quit()
    return screenshots

def send_email(style_html, script_html, gantt_html, screenshots):
    msg = MIMEMultipart()
    msg['From'] = EMAIL_ADDRESS
    msg['To'] = TO_ADDRESS
    msg['Subject'] = f"Redmine Gantt Chart - {datetime.now().strftime('%Y-%m-%d')}"

    html_body = f"""
    <html>
        <head>
            {style_html}
            <style>
                .gantt_content {{
                    overflow: auto;
                }}
                .gantt_subjects {{
                    font-size: 12px;
                }}
                .gantt-table {{
                    width: 100%;
                }}
            </style>
        </head>
        <body>
            <p>전체 간트차트 현황</p>
            <div>
                {gantt_html}
            </div>
            {script_html}
            <p>간트차트 스크린샷:</p>
    """

    for i, screenshot in enumerate(screenshots):
        img_tag = f'<img src="cid:screenshot{i}" style="display: block; margin-bottom: 20px;">'
        html_body += img_tag

    html_body += """
        </body>
    </html>
    """
    msg.attach(MIMEText(html_body, 'html'))

    for i, screenshot in enumerate(screenshots):
        img = MIMEImage(screenshot)
        img.add_header('Content-ID', f'<screenshot{i}>')
        msg.attach(img)

    server = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    server.starttls()
    server.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
    text = msg.as_string()
    server.sendmail(EMAIL_ADDRESS, TO_ADDRESS, text)
    server.quit()

def main():
    driver, style_html, script_html, gantt_html = fetch_gantt_chart_html()
    screenshots = capture_screenshots(driver)
    send_email(style_html, script_html, gantt_html, screenshots)

if __name__ == "__main__":
    main()