import re
import pandas as pd
def extract_performance_metrics(summary):
    metrics_pattern = re.compile(r'\b(\w+):\s*([\d\.]+)')
    metrics = dict(metrics_pattern.findall(summary))
    return metrics

def extract_applicable_examples(summary):
    examples_pattern = re.compile(r'Applicable examples:\s*(.+)')
    match = examples_pattern.search(summary)
    if match:
        examples = match.group(1).split(',')
        return [example.strip() for example in examples]
    return []

performance_data = []
df = pd.read_csv('latest_arxiv_papers.csv')

for _, row in df.iterrows():
    title = row['Title']
    summary = row['Summary']
    metrics = extract_performance_metrics(summary)
    examples = extract_applicable_examples(summary)
    performance_data.append((title, metrics, examples))

performance_df = pd.DataFrame(performance_data, columns=['Title', 'Metrics', 'Applicable Examples'])
print(performance_df)