import time
from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.options import Options

chrome_options = Options()
chrome_options.add_argument("--headless")
chrome_options.add_argument("--disable-gpu")
chrome_options.add_argument("--no-sandbox")
chrome_options.add_argument("--window-size=1920x1080")

# URLs to navigate
urls = [
    "https://lnkd.in/eBVdss4T",
    "https://lnkd.in/ee4gxDvT",
    "https://lnkd.in/erJaqDCK",
    "https://lnkd.in/eUcqfz_r",
    "https://lnkd.in/euqrJZz6",
    "https://lnkd.in/eDS2Af4y",
    "https://lnkd.in/e4nyJJZG",
    "https://lnkd.in/eJayUv5K",
    "https://lnkd.in/eZ5pWUqA",
    "https://lnkd.in/ewJFAfb7"
]

# Setup the WebDriver for Chrome
driver = webdriver.Chrome(service=Service(ChromeDriverManager().install()), options=chrome_options)

# Iterate through each URL
for url in urls:
    driver.get(url)
    time.sleep(5)  # Wait for page to load

    # Find all elements with anchor tags and print their href attributes
    all_links = driver.find_elements(By.TAG_NAME, 'a')
    filtered_links = [link.get_attribute('href') for link in all_links if 'https://cloudskillsboost.google/course_templates' in (link.get_attribute('href') or '')]

    print(f"Filtered links on {url}:")
    for href in filtered_links:
        print(href)

    print("\n")  # Print a newline for better separation between URLs

# Clean up and close the browser
driver.quit()
