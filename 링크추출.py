import requests
from bs4 import BeautifulSoup

# List of URLs to fetch
urls = [
    'https://lnkd.in/eBVdss4T',
    'https://lnkd.in/ee4gxDvT',
    'https://lnkd.in/erJaqDCK',
    'https://lnkd.in/eUcqfz_r',
    'https://lnkd.in/euqrJZz6',
    'https://lnkd.in/eDS2Af4y',
    'https://lnkd.in/e4nyJJZG',
    'https://lnkd.in/eJayUv5K',
    'https://lnkd.in/eZ5pWUqA',
    'https://lnkd.in/ewJFAfb7'
]

def get_page_content(url):
    """ Fetch page content for a given URL """
    try:
        response = requests.get(url)
        response.raise_for_status()  # Will raise an exception for 4XX/5XX errors
        return response.text
    except requests.RequestException as e:
        return f"Failed to fetch {url}: {str(e)}"

def extract_main_text(html_content):
    """ Extract main text from the HTML content using BeautifulSoup """
    soup = BeautifulSoup(html_content, 'html.parser')
    main_text = ' '.join(p.get_text() for p in soup.find_all('p'))
    return main_text

# Iterate over each URL, fetch the content, and print the main text
for url in urls:
    html_content = get_page_content(url)
    if html_content.startswith("Failed"):
        print(html_content)  # Print the error message
    else:
        main_text = extract_main_text(html_content)
        print("Main text from URL", url, ":\n", main_text)
