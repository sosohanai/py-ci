# pip install pandas numpy matplotlib seaborn nltk scikit-learn
import nltk
from nltk.corpus import movie_reviews
nltk.download('movie_reviews')

import pandas as pd
import random

# 데이터 로드 및 준비
def load_movie_reviews():
    reviews = []
    for fileid in movie_reviews.fileids():
        category = movie_reviews.categories(fileid)[0]
        text = " ".join(movie_reviews.words(fileid))
        reviews.append((text, category))
    return reviews

reviews = load_movie_reviews()

# 데이터 프레임 생성
df = pd.DataFrame(reviews, columns=['review', 'sentiment'])
df['sentiment'] = df['sentiment'].map({'pos': 1, 'neg': 0})

# 데이터 샘플링 (데이터가 너무 많으면 시간이 오래 걸릴 수 있습니다)
df = df.sample(500, random_state=42).reset_index(drop=True)
df.head()